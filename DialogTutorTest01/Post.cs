﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DialogTutorTest01
{
  public  class Post
    {
        public int image;
        public string title;
        public string description;


        public Post(int image, string title, string description)
        {
            this.image = image;
            this.title = title;
            this.description = description;
        }
    }


}